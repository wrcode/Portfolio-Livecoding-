$(document).ready(function () {

  $('.first-content img').addClass('animated bounceIn');

  $('.first-content h1').addClass('animated bounceInLeft');

  $('.first-content p').addClass('animated bounceInRight');

  $(window).scroll(function () {
    var height = $(window).scrollTop();
    if (height > 400) {
      $('.main-text p ').addClass('animated bounceInLeft');
    }
    if (height > 650) {
      $('.main-text h2 ').addClass('animated fadeIn');

    }
    if (height > 850) {
      $('.three-boxes .box ').addClass('animated zoomIn');
    }
    if (height > 1050) {
      $('.third-section h2 ').addClass('animated zoomIn');
    }
    if (height > 1250) {
      $('.third-section .nav ').addClass('animated zoomIn');
    }
    if (height > 1450) {
       $('.third-section .photo-blocks ').addClass('animated slideInUp');
    }
    if (height > 2450) {
      $('.third-section .btn ').addClass('animated rubberBand');
    }
     if (height > 2750) {
      $('.fourth-section h2 ').addClass('animated swing');
    }
     if (height > 2850) {
      $('.fourth-section p ').addClass('animated bounceInLeft');
    }
     if (height > 2950) {
      $('.fourth-section .photography ').addClass('animated slideInUp');
    }
     if (height > 3950) {
      $('.fourth-section .btn ').addClass('animated rubberBand');
    }
      if (height > 4150) {
      $('.fifth-content').addClass('animated zoomIn');
    }
      if (height > 4850) {
      $('.six-content').addClass('animated fadeInUp');
    }
  });
});
