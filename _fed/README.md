# Git clone
I would suggest clone FED starter into `_fed` folder localized into project root.

# Installation

**Ruby**
To install ruby go to official [website](http://rubyinstaller.org/)

**1. sass**

```sh
$ gem install sass
```

**2. compass**

```sh
$ gem install compass
```

**Node.js**
To install node.js go to official [website](http://nodejs.org/)

**1. gulp**

```sh
$ _fed/npm install --global gulp
```

**2. install node dependencies**

```sh
$ _fed/npm install
```

**3. compile all gulp tasks and download bower dependencies if needed**

```sh
$ _fed/gulp
```

### Run app

**Start file watchers**

```sh
$ _fed/gulp watch
```

### How should I use this _fed files structure properly?
**The only files you will edit** are inside `src` folder. There are sub-folders like:
  - `styles` has `main.scss` file which gonna be minify and optimized for all supported browsers
  - `scripts` / to compile, minify and concat scripts, open `gulpfile.js` and add files src into **scriptsToLoad[]** array. If you adding new file to gulp please remember to restart gulp watcher
  - `images` all `image files` in this folder gonna be compressed and optimized

Compiled files will be genarated into `../dist` folder. You can always change destination folder variable **outputPath** in `gulpfile.js`

### Feedback
If you have any problems or improvements suggestion feel free to email me rafal@nitro-digital.com

.
.
.
.
.
.
.
