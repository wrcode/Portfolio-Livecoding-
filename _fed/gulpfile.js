var outputPath = '../public/';

var scriptsToLoad = [
    'bower_components/angular/angular.js',
    'bower_components/jquery/dist/jquery.js',
    'src/scripts/main.js'
];
//
//
//// Load plugins
var gulp = require('gulp'),
    bower = require('gulp-bower'),
    compass = require('gulp-compass'),
    jade = require('gulp-jade'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    //scsslint = require('gulp-scss-lint'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    bless = require('gulp-bless'),
    webserver = require('gulp-webserver'),
    notify = require("gulp-notify");
//
//// Styles
gulp.task('styles', function () {
    return gulp.src('src/styles/main.scss')
        .pipe(compass({
            sass: 'src/styles',
            image: 'src/images',
            css: outputPath + 'css',
            logging: false,
            sourcemap: true
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'ff 17', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest(outputPath + 'css'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifycss());
    //.pipe(gulp.dest(outputPath + 'css'))

});

gulp.task('bless-conversion', ['styles'], function () {
    return gulp.src(outputPath + 'css/main.css')
        .pipe(bless())
        .pipe(gulp.dest(outputPath + 'css'))
        .pipe(notify("SCSS compiled !"));
});
//
//// Scripts
gulp.task('scripts', function () {
    return gulp.src(scriptsToLoad)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(outputPath + 'js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest(outputPath + 'js'))
        .pipe(notify("Scripts compiled!"));
});
//
//// Images
gulp.task('images', function () {
    return gulp.src('src/images/**/*')
//        .pipe(imagemin({
//            optimizationLevel: 3,
//            progressive: true,
//            interlaced: true
//        }))
        .pipe(gulp.dest(outputPath + 'img'));
});
//
//// Fonts
gulp.task('fonts', function () {
    return gulp.src('src/fonts/**/*')
        // .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest(outputPath + 'fonts'));
});
//

gulp.task('cssCopy', function () {
    return gulp.src('src/styles/**/*.css')
        // .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest(outputPath + 'css'));
});

//// Templates
gulp.task('htmlTemplates', function () {
    return gulp.src('src/templates/*.html')
        .pipe(gulp.dest(outputPath));
});
//
gulp.task('jadeTemplates', function () {
    gulp.src('src/templates/*.jade')
        .pipe(jade())
        .pipe(gulp.dest(outputPath))
        .pipe(notify("Jade compiled!"));
});
//
//// Webserver with auto reload
gulp.task('webserver', function () {
    gulp.src(outputPath)
        .pipe(webserver({
            host: 'localhost',
            port: '8001',
            livereload: true,
            directoryListing: false,
            open: true
        }))
        .pipe(notify({
            message: 'Webserver is working'
        }));
});
//
//// Clean
gulp.task('clean', function (cb) {
    del([outputPath + 'css', outputPath + 'js', outputPath + 'img', outputPath + '*.html'], {
        force: true
    }, cb)
});
//
// Bower
gulp.task('bower', function () {
    return bower();
});
//
//// Default task
//gulp.task('scss-lint', function() {
//  gulp.src('src/styles/*.scss')
//    .pipe(scsslint());
//});
gulp.task('default', ['clean', 'bower'], function () {
    gulp.start(
        //'styles',
        'bless-conversion',
        'cssCopy',
        'scripts',
        'images',
        'fonts',
        //'htmlTemplates',
        'jadeTemplates'
    );
});
//
//// Watch
gulp.task('watch', function () {
    gulp.watch('src/styles/**/*.scss', ['bless-conversion']);
    gulp.watch('src/styles/**/*.sass', ['bless-conversion']);
    gulp.watch('src/styles/**/*.css', ['cssCopy']);
    gulp.watch('src/scripts/**/*.js', ['scripts']);
    gulp.watch('src/images/**/*', ['images']);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('src/templates/*.html', ['htmlTemplates']);
    gulp.watch(['src/templates/*.jade', 'src/templates/**/*.jade'], ['jadeTemplates']);
});
